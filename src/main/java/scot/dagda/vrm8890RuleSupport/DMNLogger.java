package scot.dagda.vrm8890RuleSupport;

import org.kie.dmn.api.core.ast.DecisionNode;
import org.kie.dmn.api.core.event.AfterEvaluateBKMEvent;
import org.kie.dmn.api.core.event.AfterEvaluateDecisionEvent;
import org.kie.dmn.api.core.event.AfterEvaluateDecisionServiceEvent;
import org.kie.dmn.api.core.event.AfterEvaluateDecisionTableEvent;
import org.kie.dmn.api.core.event.AfterInvokeBKMEvent;
import org.kie.dmn.api.core.event.BeforeEvaluateBKMEvent;
import org.kie.dmn.api.core.event.BeforeEvaluateDecisionEvent;
import org.kie.dmn.api.core.event.BeforeEvaluateDecisionServiceEvent;
import org.kie.dmn.api.core.event.BeforeEvaluateDecisionTableEvent;
import org.kie.dmn.api.core.event.BeforeInvokeBKMEvent;
import org.kie.dmn.api.core.event.DMNRuntimeEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DMNLogger implements DMNRuntimeEventListener {

    //
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final String LOG_HEADER = "DMN_LOG";

    public DMNLogger() {
    }

    @Override
    public void beforeEvaluateDecision(BeforeEvaluateDecisionEvent event) {
        writeToLogInfo("BEFORE_EVALUATE_DECISION " + event.getDecision().getName());
    }

    @Override
    public void afterEvaluateDecision(AfterEvaluateDecisionEvent event) {
        DecisionNode decisionNode = event.getDecision();
        writeToLogInfo("AFTER_EVALUATE_DECISION " + event.getDecision().getName() + " DECISION_NODE " + decisionNode
                + " DECISION_RESULTS:" + event.getResult().getDecisionResults());
    }

    @Override
    public void beforeEvaluateBKM(BeforeEvaluateBKMEvent event) {
        writeToLogInfo("BEFORE_EVALUATE_BKM " + event.getBusinessKnowledgeModel().getModelName());
    }

    @Override
    public void afterEvaluateBKM(AfterEvaluateBKMEvent event) {
        writeToLogInfo("AFTER_EVALUATE_BKM " + event.getBusinessKnowledgeModel().getModelName());
    }

    @Override
    public void beforeEvaluateDecisionTable(BeforeEvaluateDecisionTableEvent event) {
        writeToLogInfo("BEFORE_EVALUATE_DECISION_TABLE " + event.getDecisionTableName());
    }

    @Override
    public void afterEvaluateDecisionTable(AfterEvaluateDecisionTableEvent event) {
        writeToLogInfo("AFTER_EVALUATE_DECISION_TABLE " + event.getDecisionTableName());
    }

    @Override
    public void beforeEvaluateDecisionService(BeforeEvaluateDecisionServiceEvent event) {
        writeToLogInfo("BEFORE_EVALUATE_DECISION_SERVICE " + event.getDecisionService().getName());
    }

    @Override
    public void afterEvaluateDecisionService(AfterEvaluateDecisionServiceEvent event) {
        writeToLogInfo("AFTER_EVALUATE_DECISION_SERVICE " + event.getDecisionService().getName());
    }

    @Override
    public void beforeInvokeBKM(BeforeInvokeBKMEvent event) {
        writeToLogInfo("BEFORE_EVALUATE_BKM " + event.getBusinessKnowledgeModel().getModelName());
    }

    @Override
    public void afterInvokeBKM(AfterInvokeBKMEvent event) {
        writeToLogInfo("AFTER_EVALUATE_BKM " + event.getBusinessKnowledgeModel().getModelName());
    }

    private void writeToLogInfo(String message) {
        logger.info(LOG_HEADER + " " + message);
    }

    private void writeToLogDebug(String message) {
        logger.debug(LOG_HEADER + " " + message);
    }

}
