package scot.dagda.vrm8890RuleSupport;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.kie.api.event.rule.AfterMatchFiredEvent;
import org.kie.api.event.rule.AgendaEventListener;
import org.kie.api.event.rule.AgendaGroupPoppedEvent;
import org.kie.api.event.rule.AgendaGroupPushedEvent;
import org.kie.api.event.rule.BeforeMatchFiredEvent;
import org.kie.api.event.rule.MatchCancelledEvent;
import org.kie.api.event.rule.MatchCreatedEvent;
import org.kie.api.event.rule.RuleFlowGroupActivatedEvent;
import org.kie.api.event.rule.RuleFlowGroupDeactivatedEvent;
import org.kie.api.runtime.rule.FactHandle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import scot.dagda.vrm8890DataModel.Audit;
import scot.dagda.vrm8890DataModel.SalesRecommendation;
import scot.dagda.vrm8890DataModel.Vehicle;

public class RuleLogger implements AgendaEventListener {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());
  private static final String LOG_HEADER = "RULES_CONTAINER_LOG";

  /*
   * AgendaEventListener methods
   */
  public void matchCancelled(MatchCancelledEvent event) {
    writeToLogDebug("MATCH_CANCELLED " + event.getMatch().getRule().getName());
  }

  public void matchCreated(MatchCreatedEvent event) {
    writeToLogDebug("MATCH_CREATED " + event.getMatch().getRule().getName());
  }

  public void afterMatchFired(AfterMatchFiredEvent event) {
    writeToLogInfo("AFTER_RULE_FIRED " + event.getMatch().getRule().getName());
    Collection<FactHandle> factList = event.getKieRuntime().getFactHandles();
    List<? extends FactHandle> matchFactsList = event.getMatch().getFactHandles();
    writeToLogInfo("AFTER_RULE_METADATA " + event.getMatch().getRule().getMetaData());
    writeToLogInfo("AFTER_RULE_DECLARATION_IDS " + event.getMatch().getDeclarationIds());
    for (String did : event.getMatch().getDeclarationIds()) {
      writeToLogInfo("AFTER_RULE_DECLARATION_VALUE ID ["+did +"] VALUE [" + event.getMatch().getDeclarationValue(did)+"]");
    }
    for (FactHandle fh : matchFactsList) {
      Object o = event.getKieRuntime().getObject(fh);
      //
      // for Vehicles try to print out SalesRecommendations and Audit records
      if (o instanceof Vehicle) {
        Vehicle v = (Vehicle) o; 
        writeToLogInfo("MATCHED_VEHICLE : " + v.toString());
        for (FactHandle ifh : factList) {
          Object io = event.getKieRuntime().getObject(ifh);
          if (io instanceof SalesRecommendation) {
            SalesRecommendation sr = (SalesRecommendation) io;
            if (sr.getVehicleId().equalsIgnoreCase(v.getVehicleId())) {
              writeToLogInfo("MATCHED_SALESRECOMMENDATION : " + sr.toString());
            }
          }
          if (io instanceof Audit) {
            Audit au = (Audit) io;
            if (au.getVehicleId().equalsIgnoreCase(v.getVehicleId())) {
              writeToLogInfo("MATCHED_AUDIT : " + au.toString());
            }
          }
        }
      }
    }
    //
    for (FactHandle fh : factList) {
      Object o = event.getKieRuntime().getObject(fh);
      writeToLogDebug("After_Fire_Object found : " + o.getClass().getName());
    }
  }

  public void afterRuleFlowGroupActivated(RuleFlowGroupActivatedEvent event) {
    writeToLogDebug("AFTER_RULE_FLOW_ACTIVATED " + event.getRuleFlowGroup().getName());
  }

  public void afterRuleFlowGroupDeactivated(RuleFlowGroupDeactivatedEvent event) {
    writeToLogDebug("AFTER_RULE_FLOW_DEACTIVATED " + event.getRuleFlowGroup().getName());
  }

  public void agendaGroupPopped(AgendaGroupPoppedEvent event) {
    writeToLogDebug("AGENDA_POPPED_OUT " + event.getAgendaGroup().getName());
  }

  public void agendaGroupPushed(AgendaGroupPushedEvent event) {
    writeToLogDebug("AGENDA_PUSHED_IN " + event.getAgendaGroup().getName());
  }

  public void beforeMatchFired(BeforeMatchFiredEvent event) {
    writeToLogDebug("FIRING_RULE " + event.getMatch().getRule().getName());
    Collection<FactHandle> factList = event.getKieRuntime().getFactHandles();
    for (Iterator<FactHandle> iterator = factList.iterator(); iterator.hasNext();) {
      FactHandle fh = iterator.next();
      Object o = event.getKieRuntime().getObject(fh);
      writeToLogDebug("Before_Fire_Object found : " + o.getClass().getName());
    }
  }

  public void beforeRuleFlowGroupActivated(RuleFlowGroupActivatedEvent event) {
    writeToLogDebug("BEFORE_RULE_FLOW_ACTIVATED " + event.getRuleFlowGroup().getName());
  }

  public void beforeRuleFlowGroupDeactivated(RuleFlowGroupDeactivatedEvent event) {
    writeToLogDebug("BEFORE_RULE_FLOW_DEACTIVATED " + event.getRuleFlowGroup().getName());
  }

  private void writeToLogInfo(String message) {
    logger.info(LOG_HEADER + " " + message);
  }

  private void writeToLogDebug(String message) {
    logger.debug(LOG_HEADER + " " + message);
  }

}
